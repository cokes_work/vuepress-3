module.exports = {
    title: 'GitLab ❤️ VuePress ❤️ cokes3',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'
}